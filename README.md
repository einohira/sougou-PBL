# [Name of this software]

## Overview

カメラで人を検知すると話し掛ける対話システムです。

たとえば、帰宅後カメラが人を検知するとおかえりと話し掛けてきます。それに対してただいまと返事をすると、その後どうしたのやねえねえと話し掛けてきます。

また、ユーザーがある特定の言葉で話しかけると返事します。
朝と帰宅後を想定した会話パターンを作成します。



--

It is a dialogue system that talks when detecting a person with a camera.

For example, when the camera senses a person after coming home, this system will talk to user "okaeri"(Welcome back).
If user reply to it “tadaima”(I’m home), this system will talk to him “dousitano?”(What happened?) or ”nene”(HeyHey) , and so on.

Moreover , this system will reply when the user talks to a certain word.
There is a conversation pattern assuming morning and after coming home.
(This system can speak Japanese only.)




## System requirements

### Hardware

* PC
* WebCam
* Microphone
* Speacker

### Software

* Python 3
* Javascript
* Web browser

### Dependencies

Main modules

* OpenCV
* Google Cloud Speech-to-Text API, Cloud Text-to-Speech API
* Websocket
* transitions (finite state machine)
* Web Audio API
  
## Install

### Windows

Python3

1. Download an installer from https://www.python.org/downloads/windows/
2. Install Python 3


### linux



```
sudo apt install libasound-dev portaudio19-dev libportaudiocpp0D 
```

## Google Cloud SDK setup
This software needs an account of Google Cloud Plaform for Google Cloud Speech-to-Text API and Cloud Text-to-Speech API.

Install Google Could SDK

https://cloud.google.com/sdk/downloads?hl=ja

```
gcloud init
gcloud auth application-default login
```

## clone this repository

```
git clone https://xxx
```

## Setup a virtual environment for python

```
python -m venv [target dir]
```

Windows

```
Scripts\activate
```

Linux

```
bin\activate
```

## Install python modules

```
pip install -r requirements.txt
```

## Launch

```
python main.py
```

When you would like to finish the script, input "exit" to the terminal.

## How to use 

学校休みたいと話し掛ければがんばってと言います。
いい天気と呟けばお出かけする？と言います。
時間が知りたいときには何時と聞けば、現在時刻を教えてくれます。

もし、ユーザーが無視すればどうしたの？と一定時間後に話し掛けます。それでもユーザーが無視した場合には会話を終了します。返事が返ってきた場合には通常の会話に戻ります。

--

User : "gakkouyasumitai"(I would like to take a day off from school)
system : "gannbatte"( Good luck)

User : “iitennki"(Nice weather today)
System :  "odekakesuru?"(Would you like to go?)
If user want to ask what time is it now.
User : “nannji?"( what time is it now?)
System :  "nijizerohunndesu"(it is two o’clock)

If the user ignores it, I will speak to you after a certain time as "dousitano?”(What's wrong?). If the user still ignores it, the conversation ends. 
An answer came back In case you return to usual conversation.


## Web browser

Open a webpage
"http://127.0.0.1:8080/voice/test06.html"


Input the following URL to a textbox
"ws://127.0.0.1:9999"

## License

This software is released under the MIT License except voice/transcribe_streaming_mic.py.
See License.
