#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.



"""Google Cloud Speech API sample application using the streaming API.

NOTE: This module requires the additional dependency `pyaudio`. To install
using pip:

    pip install pyaudio

Example usage:
    python transcribe_streaming_mic.py
"""
# Copyright 2018 Saki Matsuura
# This software may be modified and distributed under the terms
# of the MIT license

# [START speech_transcribe_streaming_mic]
from __future__ import division

import re
import sys

from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
import pyaudio
from six.moves import queue
#from websocket import create_connection


# Audio recording parameters
RATE = 16000
CHUNK = int(RATE / 10)  # 100ms


class MicrophoneStream(object):
    """Opens a recording stream as a generator yielding the audio chunks."""
    def __init__(self, rate, chunk):
        self._rate = rate
        self._chunk = chunk

        # Create a thread-safe buffer of audio data
        self._buff = queue.Queue()
        self.closed = True

    def __enter__(self):
        self._audio_interface = pyaudio.PyAudio()
        self._audio_stream = self._audio_interface.open(
            format=pyaudio.paInt16,
            # The API currently only supports 1-channel (mono) audio
            # https://goo.gl/z757pE
            channels=1, rate=self._rate,
            input=True, frames_per_buffer=self._chunk,
            # Run the audio stream asynchronously to fill the buffer object.
            # This is necessary so that the input device's buffer doesn't
            # overflow while the calling thread makes network requests, etc.
            stream_callback=self._fill_buffer,
        )

        self.closed = False

        return self

    def __exit__(self, type, value, traceback):
        self._audio_stream.stop_stream()
        self._audio_stream.close()
        self.closed = True
        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.
        self._buff.put(None)
        self._audio_interface.terminate()

    def _fill_buffer(self, in_data, frame_count, time_info, status_flags):
        """Continuously collect data from the audio stream, into the buffer."""
        self._buff.put(in_data)
        return None, pyaudio.paContinue

    def generator(self):
        while not self.closed:
            # Use a blocking get() to ensure there's at least one chunk of
            # data, and stop iteration if the chunk is None, indicating the
            # end of the audio stream.
            chunk = self._buff.get()
            if chunk is None:
                return
            data = [chunk]

            # Now consume whatever other data's still buffered.
            while True:
                try:
                    chunk = self._buff.get(block=False)
                    if chunk is None:
                        return
                    data.append(chunk)
                except queue.Empty:
                    break

            yield b''.join(data)


def listen_print_loop(responses):
    """Iterates through server responses and prints them.

    The responses passed is a generator that will block until a response
    is provided by the server.

    Each response may contain multiple results, and each result may contain
    multiple alternatives; for details, see https://goo.gl/tjCPAU.  Here we
    print only the transcription for the top alternative of the top result.

    In this case, responses are provided for interim results as well. If the
    response is an interim one, print a line feed at the end of it, to allow
    the next result to overwrite it, until the response is a final one. For the
    final one, print a newline to preserve the finalized transcription.
    """
    num_chars_printed = 0
    for response in responses:
        if not response.results:
            continue

        # The `results` list is consecutive. For streaming, we only care about
        # the first result being considered, since once it's `is_final`, it
        # moves on to considering the next utterance.
        result = response.results[0]
        if not result.alternatives:
            continue

        # Display the transcription of the top alternative.
        transcript = result.alternatives[0].transcript

        # Display interim results, but with a carriage return at the end of the
        # line, so subsequent lines will overwrite them.
        #
        # If the previous result was longer than this one, we need to print
        # some extra spaces to overwrite the previous result
        overwrite_chars = ' ' * (num_chars_printed - len(transcript))

        

        if not result.is_final:
            sys.stdout.write(transcript + overwrite_chars + '\n')
            sys.stdout.flush()

            num_chars_printed = len(transcript)
        


        else:
            print(transcript + overwrite_chars)

            # Exit recognition if any of the transcribed phrases could be
            # one of our keywords.
            # if re.search(r'こんにちは', transcript, re.I):
            #     print('!!!こんにちは!!!')
            #     break

            num_chars_printed = 0

        if re.search(r'おはよう', transcript, re.I):
        #    ws.send('おはよう')
            #ws.send('ohayou')          
            return 'ohayou'
            #break
        if re.search(r'おやすみ', transcript, re.I):
        #    ws.send('おやすみ')
            #ws.send('oyasumi')
            #break
            return 'oyasumi'
        if re.search(r'学校休みたい', transcript, re.I):
            #ws.send('学校休みたい')
            #break
            return 'gakkouyasumitai'

        if re.search(r'ただいま', transcript, re.I):
            # ws.send('tadaima')
            # break
            return 'tadaima'

        if re.search(r'楽しかった', transcript, re.I):
            # ws.send('tanosikatta')
            # break
            return 'tanosikkata'
        if re.search(r'いい天気', transcript, re.I):
            # ws.send('iitennki')
            # break
            return 'iitennki'
        if re.search(r'お腹すいた', transcript, re.I):
            # ws.send('onakasuita')
            # break
            return 'onakasuita'
        if re.search(r'寒い', transcript, re.I):
            # ws.send('samui')
            # break
            return 'samui'
        if re.search(r'暑い', transcript, re.I):
            # ws.send('atui')
            # break
            return 'atui'
        if re.search(r'ねえねえ', transcript, re.I):
            # ws.send('nene')
            # break
            return 'nene'
        if re.search(r'疲れた', transcript, re.I):
            # ws.send('tukareta')
            # break
            return 'tukareta'
        if re.search(r'何時', transcript, re.I):
            # ws.send('nannji')
            # break
            return 'nannji'
def main(q):
    # See http://g.co/cloud/speech/docs/languages
    # for a list of supported languages.
    language_code = 'ja-JP'  # a BCP-47 language tag

    client = speech.SpeechClient()
    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        sample_rate_hertz=RATE,
        language_code=language_code)
    streaming_config = types.StreamingRecognitionConfig(
        config=config,
        interim_results=True)


    #ws = create_connection("ws://127.0.0.1:9999/")

    while True:
        print("録音中")
        with MicrophoneStream(RATE, CHUNK) as stream:
            audio_generator = stream.generator()
            requests = (types.StreamingRecognizeRequest(audio_content=content)
                        for content in audio_generator)

            responses = client.streaming_recognize(streaming_config, requests)

            # Now, put the transcription responses to use.

            try:
                msg = listen_print_loop(responses)
                if msg is not None:
                    q.put('音声:'+msg)
            except:
                print("sr restart")

    #ws.close()    


    

   

if __name__ == '__main__':
    main()
# [END speech_transcribe_streaming_mic]
