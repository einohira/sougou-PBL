#!/usr/bin/python
# -*- coding: utf-8 -*-
# Eiichi Inohira, 2018

# For python 3, windows

import urllib.request
import subprocess
import json
import base64
import webbrowser
import os
import time

from websocket import create_connection

def main():

      ws = create_connection("ws://192.168.0.2:9999/")



      # 音声合成のためのURL + 認証のためのAPI KEY
      # 
      g_url = "https://texttospeech.googleapis.com/v1beta1/text:synthesize?key=AIzaSyA4cCa3vYf8P4C1IMvrr31Uc1ml6YrpyGI"

      # HTTPリクエストのためのヘッダ
      # Referer: 呼び出し元のウェブサイト（API KEYのアクセス制限に使用）
      hd = {'Content-Type': 'application/json; charset=utf-8',
            'Referer': 'http://www.sougoupbl.kyutech.ac.jp/'}

      while True:
            result =  ws.recv()

            if len(result)>100:
                  print('文章が長すぎます')
                  continue
            print(result)

            # texttospeech.googleapis.comにHTTP POSTで送るJSONデータ（入力テキスト、音声（言語）の種類、出力形式）
            msg = json.dumps({'input': {'text': result}, 'voice': {
                        'languageCode': 'ja-JP'}, 'audioConfig': {'audioEncoding': 'MP3'}})

            # 1. HTTPリクエストのためのオブジェクトを生成
            req = urllib.request.Request(url=g_url, data=msg.encode(), headers=hd)

            # 2. URLを開く（reqオブジェクトを使ってJSONデータを送る）
            f = urllib.request.urlopen(req)

            # 3. サーバからの応答メッセージ(JSON形式)を読む
            res = f.read()

            # 4. JSON形式のデータをPythonオブジェクトに変換
            x = json.loads(res)

            # 5. キーaudioContentの値をデコード
            # サーバが出力音声をMP3形式に変換したデータをBASE64形式でテキストに変換して送信してくるので復元する
            y = base64.b64decode(x['audioContent'])


            # 6. 変換したデータをMP3ファイルとして保存
            with open('output.mp3', 'wb') as out:
                  out.write(y)
                  print('Audio content written to file "output.mp3"')

            # 7. MP3ファイルをchromeで再生
            # b = webbrowser.get('C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s')
            # b = webbrowser.get('cmd /c start microsoft-edge:%s')
            # b.open('file://'+os.getcwd()+'/output.mp3')

      ws.close()

if __name__ == "__main__":
      main()