# Copyright 2018 Minami Mako
# This software may be modified and distributed under the terms
# of the MIT license

import cv2

#from websocket import create_connection

def main(q):

    #ws = create_connection("ws://127.0.0.1:9999/")
    
    # 定数定義
    ESC_KEY = 27     # Escキー
    INTERVAL= 33     # 待ち時間
    FRAME_RATE = 30  # fps

    ORG_WINDOW_NAME = "org"
    GAUSSIAN_WINDOW_NAME = "gaussian"


    DEVICE_ID = 0

    # 分類器の指定
    cascade_file = "sensor/haarcascade_frontalface_alt2.xml"
    cascade = cv2.CascadeClassifier(cascade_file)
    if cascade.empty() == True:
        print("not load classifier")
        #ws.close()
        return


    # カメラ映像取得
    print("waiting for VideoCapture()")
    cap = cv2.VideoCapture(DEVICE_ID)
    if not cap.isOpened():
        print("not open camera device")
        #ws.close()
        return
    print("got VideoCapture()")

    # 初期フレームの読込
    end_flag, c_frame = cap.read()
    height, width, channels = c_frame.shape

    # ウィンドウの準備
    #cv2.namedWindow(ORG_WINDOW_NAME)
    cv2.namedWindow(GAUSSIAN_WINDOW_NAME)

    a = 0
    b = 0
    count = 0

    print("sensor start")
    # 変換処理ループ
    while end_flag == True:

        # 画像の取得と顔の検出
        img = c_frame
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        face_list = cascade.detectMultiScale(img_gray, minSize=(100, 100))

        if len(face_list) > 0:
            b = 1
        else:
            b = 0

        if (a == 0) and (b == 1):                
            if(count == 0):
                q.put('sensor:1')
                count = 30


        if (a == 1) and (b == 0):
            if(count == 0):
                q.put('sensor:0')
                count = 30

        a = b


        # 検出した顔に印を付ける
        for (x, y, w, h) in face_list:
            color = (0, 0, 225)
            pen_w = 3
            cv2.rectangle(img_gray, (x, y), (x+w, y+h), color, thickness = pen_w)

        # フレーム表示
        #cv2.imshow(ORG_WINDOW_NAME, c_frame)
        cv2.imshow(GAUSSIAN_WINDOW_NAME, img_gray)

        # Escキーで終了
        key = cv2.waitKey(INTERVAL)
        if key == ESC_KEY:
            break

        # 次のフレーム読み込み
        end_flag, c_frame = cap.read()

        if(count > 0):
            count-=1


    # 終了処理
    cv2.destroyAllWindows()
    cap.release()
    #ws.close()

if __name__ == '__main__':
    pass