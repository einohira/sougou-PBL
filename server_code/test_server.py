#!/usr/bin/env python
# -*- coding: utf-8 -*-

from websocket_server import WebsocketServer

clients =['id']

# 新しいクライアントに接続したことを通知し、IDを振る関数
def new_client(client, server):
	print("New client connected and was given id %d" % client['id'])
	clients.append(client)

# クライアントとの接続が切れたことを通知して、IDを消す
def client_left(client, server):
	print("Client(%d) disconnected" % client['id'])
	client.remove(client)

# メッセージを送られたときに起きる関数
def message_received(client, server, message):
	if len(message) > 200:
		message = message[:200]+'..'
	print("Client(%d) sent: %s" % (client['id'], message)) #4:センサ,3:音声認識,2:制御,1:発話
	#センサーからの信号を制御に送る
	if client['id'] == 4:
		server.send_message(clients[2],'センサ:'+message)
	#制御からの信号を発話に送る
	if client['id'] == 2:
		server.send_message(clients[1],message)
	#音声認識からのデータを制御に送る
	if client['id'] == 3:
		server.send_message(clients[2],'音声:'+message)

	


server = WebsocketServer(9999, host='0.0.0.0')
server.set_fn_new_client(new_client) #クライアントが接続したときに関数new_clientを呼ぶ
server.set_fn_client_left(client_left) #クライアントとの接続が切れた時に関数client_leftを呼ぶ
server.set_fn_message_received(message_received) #クライアントからメッセージを受け取ったときに関数message_receivedを呼ぶ
server.run_forever()