#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright 2018 Shizuka Tamura
# This software may be modified and distributed under the terms
# of the MIT license

import time
import datetime
import random
import threading
from transitions import Machine
#from websocket import create_connection

'''
Script: ???
'''


#self.q = create_connection("self.q://192.168.0.2:9999/")
#vprint('コネクト')

class Matter(object):
    
    '''
ex    class Matter:  何とかクラス
    '''
    def oha(self):
        '''
        何とか関数
        '''
        time.sleep(1)
        self.q.put('おはよう')
        self.reset_tr()
        print (self.state)

    def oya(self):
        time.sleep(1)
        self.q.put('おやすみ')
        self.reset_tr()
        print (self.state)

    def gannbatte(self):
        time.sleep(1)
        self.q.put('gannbatte')

    def ittera(self):
        ##self.q.put('itterassyai')
        ##self.reset_tr()
        ##print (self.state)
        x = random.randrange(3)

        if x == 0:
            time.sleep(1)
            self.q.put('itterassyai')
            print('itterassyai')
            self.reset_tr()
            print(self.state)
            ##lump.tr_ittera()

        if x == 1: 
            time.sleep(1)
            self.q.put('kiwotukete')
            print('kiwotukete')
            self.reset_tr()
            print(self.state)
            ##lump.tr_kiwotuketene()

        if x == 2:
            time.sleep(1)
            self.q.put('gannbatte')
            print('gannbatte')
            self.reset_tr()
            print(self.state)
            ##lump.tr_gannbatte()

    def okaeri(self):
        time.sleep(1)
        self.q.put('okaeri')
        print('okaeri')
        #self.time_tr()
        print (self.state)      #(lump.state)→(self.state)

    def otukare(self):
        time.sleep(1)
        self.q.put('otukare')
        print('otukare')
        #self.time_tr()
        print (self.state) 

    def hima(self,timer=None):
        print('hima', timer)

        x = random.randrange(1)         #発音させるように範囲狭めた
        

        if x == 1:
            time.sleep(4)
            self.q.put('ne-ne')
            print('ne-ne')

        elif x == 0:
            time.sleep(4)
            self.q.put('dousitano?')
            print('dousitano')
            
        if timer is None:
            print('timer')
            self.t = threading.Timer(5,self.hima,kwargs={'timer':1})
            self.t.start()                          #次の課題：timerが起動中に他のトリガーがかかったらtimerが停止されるようにする
                                                    
        
        else:
            self.reset_tr()
            print('reset')





    def hennji(self):
        x = random.randrange(2)

        if x == 0:
            time.sleep(1)
            self.q.put('sounannya')        #そうなんや
            #self.time_tr()
        
        elif x == 1:
            time.sleep(1)
            self.q.put('gannbaroune')      #がんばろうね
            #self.time_tr()

    def weather(self):
        x == random.randrange(21)

        if x >= 0 and x <= 7:
            self.q.put('odekakesuru?')
            self.time_tr()

        elif x >= 8 and x >= 15:
            self.q.put('senntakusita?')
            self.time_tr()

        elif x >= 16 and x<= 19:
            self.q.put('soudesune')
            self.time_tr()

        if x == 20:
            self.q.put('ariyorinoari')
            self.time_tr()

    def gohan(self):
        x = random.randrange(21)

        if x >= 0 and x <= 11:
            self.q.put('gohantabeyou')     #ごはん食べた？
            self.time_tr()
        
        elif x >= 12 and x <= 15:
            self.q.put('sounannda')        #そうなんだ
            self.time_tr()
        
        elif x >= 16 and x <= 19:
            self.q.put('iine')     #いいね
            self.time_tr()
        
        elif x == 20:
            self.q.put('ariyorinoari')
            self.time_tr()

    def samui(self):
        x = random.randrange(21)

        if x >= 0 and x <= 17:
            self.q.put('kazehikanaiyounine')     #風邪ひかないようにね
            #self.time_tr()
          
        elif x >= 18 and x <= 19:
            self.q.put('sounannda')     #そうなんだ
            #self.time_tr()

        elif x == 20:
            self.q.put('ariyorinoari')
            #self.time_tr()
        '''
        if x == :
            self.q.put('soudesune')
            self.time_tr()
        '''

    def atui(self):
        x = random.randrange(21)

        if x >= 0 and x <= 14:
            self.q.put('suibunnhokyuudaiji')
            #self.time_tr()
        
        elif x >= 15 and x <= 19:
            self.q.put('sounannda')
            #self.time_tr()

        elif x == 20:
            self.q.put('ariyorinoari')
            #self.time_tr()
        '''
        if x == :
            self.q.put('')
            self.time_tr()
        '''
    
    def tukareta(self):
        x == random.randrange(21)

        if x >= 0 and x <=9:
            self.q.put('yokugannbattene')
            #self.time_tr()
        
        elif x >= 10 and x <= 19:
            self.q.put('eraine')
            #self.time_tr()

        elif x == 20:
            self.q.put('ariyorinoari')
            #self.time_tr()

    def nene(self):
        x = random.randrange(10)

        if x >= 0 and x <=2:
            self.q.put('naani')
            #self.time_tr()
        
        elif x >= 3 and x <= 5:
            self.q.put('unn')
            #self.time_tr()
        
        elif x >= 6 and x <= 8:
            self.q.put('dousitano')
            #self.time_tr()

        elif x == 9:
            self.q.put('ariyorinoari')
            #self.time_tr()

    def jikann(self):
        time = datetime.datetime.now()
        
        hour = time.hour
        minute = time.minute

        self.q.put('{0}時{1}分'.format(hour,minute))


    
    def sorena(self):
        time.sleep(1)
        self.q.put('sorena')
        self.reset_tr()

    def set_q(self,q):
        self.q = q

def main(qi, qo):

    lump = Matter()
    lump.set_q(qo)

    states = ['wait','sensor','voice','time']

    transitions = [

        { 'trigger': 'tr_oha', 'source': 'wait', 'dest': 'voice' , 'after': 'oha' },
        { 'trigger': 'tr_oha', 'source': 'sensor', 'dest': 'voice' , 'after': 'oha' },
        { 'trigger': 'tr_oya', 'source': 'wait', 'dest': 'wait' , 'after': 'oya' },
        { 'trigger': 'tr_oya', 'source': 'time', 'dest': 'wait' , 'after': 'oya' },
        { 'trigger': 'tr_gannbatte', 'source': 'wait', 'dest': 'voice' ,'after': 'gannbatte'},
        { 'trigger': 'tr_okaeri', 'source': 'wait', 'dest':'sensor','after': 'okaeri' },
        { 'trigger': 'tr_otukare', 'source': 'wait', 'dest':'sensor','after': 'otukare' },
        { 'trigger': 'tr_ittera', 'source': 'wait', 'dest':'sensor' , 'after': 'ittera' },
        { 'trigger': 'tr_0', 'source': 'wait', 'dest': 'sensor' ,'after': 'ittera'},
        { 'trigger': 'tr_0', 'source': 'time', 'dest': 'sensor' ,'after': 'ittera'},
        { 'trigger': 'tr_0', 'source': 'sensor', 'dest': 'sensor' ,'after': 'ittera'},


        { 'trigger': 'time_tr', 'source': 'sensor', 'dest': 'time' , 'after':'hima' },
        { 'trigger': 'time_tr', 'source': 'time', 'dest': 'time' , 'after':'hima' },
        { 'trigger': 'time_tr', 'source': 'wait', 'dest': 'time' , 'after':'hima' },
        { 'trigger': 'hennji_tr', 'source': 'time', 'dest': 'time' , 'after':'hennji' },
        { 'trigger': 'hennji_tr', 'source': 'sensor', 'dest': 'time' , 'after':'hennji' },
        { 'trigger': 'reset_tr', 'source': 'time', 'dest': 'wait' },
        { 'trigger': 'reset_tr', 'source': 'voice', 'dest': 'wait' },
        { 'trigger': 'reset_tr', 'source': 'sensor', 'dest': 'wait' },

        { 'trigger': 'tr_oha', 'source': 'time', 'dest': 'voice' , 'after': 'oha' },
        { 'trigger': 'tr_otukare', 'source': 'sensor', 'dest':'sensor','after': 'otukare' },
        { 'trigger': 'tr_otukare', 'source': 'time', 'dest':'sensor','after': 'otukare' },
        { 'trigger': 'tr_okaeri', 'source': 'sensor', 'dest':'sensor','after': 'okaeri' },
        { 'trigger': 'tr_okaeri', 'source': 'time', 'dest':'sensor','after': 'okaeri' },

        { 'trigger': 'tr_sorena', 'source': 'wait', 'dest': 'wait' , 'after':'sorena'},
        { 'trigger': 'reset_tr', 'source': 'wait', 'dest': 'wait' },
        
        { 'trigger': 'tr_weather', 'source': 'sensor', 'dest': 'time' , 'after':'weather'},
        { 'trigger': 'tr_gohan', 'source': 'sensor', 'dest': 'time' , 'after':'gohan'},
        { 'trigger': 'tr_samui', 'source': 'sensor', 'dest': 'time' , 'after':'samui'},

        { 'trigger': 'tr_samui', 'source': 'wait', 'dest': 'time' , 'after':'samui'},
        { 'trigger': 'tr_samui', 'source': 'time', 'dest': 'time' , 'after':'samui'},  #4限追加

        { 'trigger': 'tr_atui', 'source': 'sensor', 'dest': 'wait' , 'after':'atui'},

        { 'trigger': 'tr_atui', 'source': 'wait', 'dest': 'wait' , 'after':'atui'},
        { 'trigger': 'tr_atui', 'source': 'time', 'dest': 'wait' , 'after':'atui'},  #4限追加


        { 'trigger': 'tr_tukareta', 'source': 'sensor', 'dest': 'time' , 'after':'tukareta'},
        { 'trigger': 'tr_nene', 'source': 'sensor', 'dest': 'time' , 'after':'nene'},
        { 'trigger': 'tr_jikann', 'source': 'sensor', 'dest': 'time' , 'after':'jikann'},
        { 'trigger': 'tr_jikann', 'source': 'wait', 'dest': 'wait' , 'after':'jikann'},
        { 'trigger': 'tr_jikann', 'source': 'time', 'dest': 'wait' , 'after':'jikann'}

    ]

    machine = Machine(lump, states=states, transitions=transitions, initial='wait')

    print('control start')
    while True:
        msg = qi.get()

        print("c:%s, s:%s" % (msg,lump.state))

        try:
            # if msg == '音声:おはよう':
            if msg == '音声:ohayou':    #おはよう
                lump.tr_oha()

            elif msg == '音声:oyasumi':     #おやすみ
                lump.tr_oya()
            
            elif msg == '音声:gakkouyasumitai':     #学校休みたい
                lump.tr_gannbatte()

            elif msg == '音声:tadaima':     #ただいま
                lump.tr_hima()

            elif msg == '音声:tanosikatta':     #楽しかった
                lump.tr_hennji()
            
            elif msg == '音声:iitennki':        #いい天気
                lump.tr_weather()

            elif msg == '音声:onakasuita':      #お腹すいた
                lump.tr_gohan()

            elif msg == '音声:samui':       #寒い
                lump.tr_samui()

            elif msg == '音声:atui':        #暑い
                lump.tr_atui()

            elif msg == '音声:nene':        #人が暇で話し掛ける
                lump.tr_nene()

            elif msg == '音声:tukareta':        #疲れた
                lump.tr_tukareta()

            elif msg == '音声:nannji':      #何時
                lump.tr_jikann()

            elif msg == '音声:tanosikatta':     #
                lump.tr_hennji()

            elif msg == '音声:iitennki':        #いい天気
                lump.tr_weather()

            elif msg == '音声:onakasuita':      #お腹すいた
                lump.tr_gohan()

            elif msg == '音声:samui':       #寒い
                lump.tr_samui()

            elif msg == '音声:atui':        #暑い
                lump.tr_atui()

            elif msg == '音声:nene':        #人が暇で話し掛ける
                lump.tr_nene()

            elif msg == '音声:tukareta':        #疲れた
                lump.tr_tukareta()

            elif msg == 'sensor:0':     #
                lump.tr_0()

            elif msg == 'sensor:1':  
                x = random.randrange(2)

                if x == 0:
                    lump.tr_okaeri()        

                elif x == 1: 
                    lump.tr_otukare()

            else:
                lump.tr_sorena()        #
        except:
            print('ignore trigger')

if __name__ == '__main__':
    pass